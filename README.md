# Simple GitHub App
This is a light application, developed with _[Flask](https://github.com/pallets/flask)_.
Specially for _Smartwork Solutions_.


## Installation
Application was developed and tested with _[Python 3.6.4](https://www.python.org/downloads/release/python-364/)_, therefore this version is recommended for a usage with this app.
However higher version of Python should be satisfactorily as well.

Start from cloning this repository:
    
    $ git clone https://godis_michael@bitbucket.org/godis_michael/smartwork-api.git
    $ cd smartwork-api

Install required python modules:

    $ pip install -r requirements.txt

Note, it is preffered to install all depedencies inside virtual environment, because of a way how flask works. 
You may need also to install python3-flask if you are running on Linux:

    $ sudo apt install python3-flask

This app uses variables from `.env` file, so before running it create this file and set following variables:

* `SECRET_KEY` - application secret key
* `JWT_SECRET_KEY` - secret key for JWT generation
* `DATABASE` - database name (i.e. flaskr)

As optional variables you may set:

* `GITHUB_ACCESS_TOKEN` - github access token for authentication

    or

* `GITHUB_USERNAME` - github login
* `GITHUB_PASSWORD` - github password

Github credentials, if set, will allow you to fetch more data from api.

## Running
Before staring server, there is a need to initialize database and fulfill it with data from github.
Firstly, export all required variables:
    
    $ export FLASK_APP=flaskr
    $ export FLASK_ENV=development
    $ export FLASK_SKIP_DOTENV=1
        
or use `set` command for Windows. 
        
Now initialize database:

    $ flask init-db

This command will create all required tables and will recreate database if db is not already empty, be aware of that!

To load data from github, we can use following command:

    $ flask import --stars:optional --language:optional

By default, python packages that have more than 500 stars reputation are loaded, but you can change this by setting `--stars` and `--language` parameters.

Also, there is a possibility to load packages from the api`s endpoint, but in this case superuser creation is required:

    $ flask create-superuser --username:optional --password:optional

If no `--usernname` or `--password` parameters set, you will be asked to enter them via the command line prompt.

Now, it is time to run our app:

    $ flask run
    
For a future, you may just use `run.sh` or `run.bat` scripts to set all variables automatically and run application.

## Endpoints
Application consists of the following endpoints:

* **POST /api/login**
    
    Logs a user in and creates `access_token` for interacting with `/api/repositories/import` endpoint.
    Requires `username` and `password` parameters in a request body.
        
* **GET /api/repositories**

    Returns portion (paginated) of all available repositories.
    Accepts `sort` (_asc_ by default), `order` (_id_ by default) and `page` (_1_ by default) parameters.
    
* **GET /api/repositories/<id>**

    Returns specific repository entry.

* **GET /api/repositories/import**

    Starts github repositories load.
    Requires `access_token` in Authorization header.
    Accepts `drop` (_false_ by default), `stars` (_500_ by default) and `langage` (_python_ by default) parameters.

In addition, all endpoints have error handling, so most errors should be returned as JSON response.

## Example usage
Retrieving data after cli commands:
    
    > flask init-db
    Initialized the database
    
    > flask import --stars 500 --language java 
      [----#-------------------------------]
    Warning! API rate limit exceeded for ***.***.***.***. (But here's the good news: Authenticated requests get a higher rate limit. Check out the documentation for more details.) (https://developer.github.com/v3/#rate-limiting)
    300 entries successfully added
    
    > run.bat
     * Serving Flask app "flaskr" (lazy loading)
     * Environment: development
     * Debug mode: on
     * Restarting with stat
     * Debugger is active!
     * Debugger PIN: ***-***-***
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    
    > curl "http://localhost:5000/api/repositories?order=desc&page=3"
    [
        {
            "id": 280,
            "full_name": "weibocom/motan",
            "html_url": "https://github.com/weibocom/motan",
            "description": "A cross-language remote procedure call(RPC) framework for rapid development of high performance distributed services.",
            "stargazers_count": 4951,
            "language": "Java"
        },
        {
            "id": 279,
            "full_name": "smuyyh/BookReader",
            "html_url": "https://github.com/smuyyh/BookReader",
            "description": ":closed_book:  \"\u4efb\u9605\" \u7f51\u7edc\u5c0f\u8bf4\u9605\u8bfb\u5668\uff0c\u4eff\u771f\u7ffb\u9875\u6548\u679c\u3001txt/pdf/epub\u4e66\u7c4d\u9605\u8bfb\u3001Wifi\u4f20\u4e66~",
            "stargazers_count": 4969,
            "language": "Java"
        },
        {
            "id": 278,
            "full_name": "square/moshi",
            "html_url": "https://github.com/square/moshi",
            "description": "A modern JSON library for Kotlin and Java.",
            "stargazers_count": 4973,
            "language": "Java"
        },
        {
            "id": 277,
            "full_name": "prolificinteractive/material-calendarview",
            "html_url": "https://github.com/prolificinteractive/material-calendarview",
            "description": "A Material design back port of Android's CalendarView",
            "stargazers_count": 4986,
            "language": "Java"
        },
        {
            "id": 276,
            "full_name": "wyouflf/xUtils",
            "html_url": "https://github.com/wyouflf/xUtils",
            "description": "android orm, bitmap, http, view inject...  \u5df2\u8f6c\u81f3 https://github.com/wyouflf/xUtils3",
            "stargazers_count": 5014,
            "language": "Java"
        },
        {
            "id": 275,
            "full_name": "aa112901/remusic",
            "html_url": "https://github.com/aa112901/remusic",
            "description": "\u4eff\u7f51\u6613\u4e91\u97f3\u4e50 \u5b89\u5353\u7248\uff0cnetease  android\uff0c\u97f3\u4e50\u64ad\u653e\u5668 \u5728\u7ebf \u4e0b\u8f7d",
            "stargazers_count": 5019,
            "language": "Java"
        },
        {
            "id": 274,
            "full_name": "apache/cassandra",
            "html_url": "https://github.com/apache/cassandra",
            "description": "Mirror of Apache Cassandra",
            "stargazers_count": 5056,
            "language": "Java"
        },
        {
            "id": 273,
            "full_name": "react-native-community/react-native-image-picker",
            "html_url": "https://github.com/react-native-community/react-native-image-picker",
            "description": ":sunrise_over_mountains: A React Native module that allows you to use native UI to select media from the device library or directly from the camera.",
            "stargazers_count": 5103,
            "language": "Java"
        },
        {
            "id": 272,
            "full_name": "evernote/android-job",
            "html_url": "https://github.com/evernote/android-job",
            "description": "Android library to handle jobs in the background.",
            "stargazers_count": 5126,
            "language": "Java"
        },
        {
            "id": 271,
            "full_name": "sockeqwe/mosby",
            "html_url": "https://github.com/sockeqwe/mosby",
            "description": "A Model-View-Presenter / Model-View-Intent library for modern Android apps",
            "stargazers_count": 5128,
            "language": "Java"
        }
    ]
    
Retrieving data after endpoint command:
    
    > flask init-db
    ['repository', 'user'] tables already exist, drop them? [Y/n]: y
    Initialized the database
    
    > flask create-superuser --username admin --password admin123
    Successfully created
    
    > run.bat
     * Serving Flask app "flaskr" (lazy loading)
     * Environment: development
     * Debug mode: on
     * Restarting with stat
     * Debugger is active!
     * Debugger PIN: ***-***-***
     * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
    
    > curl -d "username=admin&password=admin123" -X POST "http://localhost:5000/api/login"
    {
        "message": "Logged in as admin",
        "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NTQ3NzE3MDMsIm5iZiI6MTU1NDc3MTcwMywianRpIjoiZmYyZmQ4OTAtZDlmYy00NDIxLWEyMWQtNTUxMTczMGNhOGY0IiwiZXhwIjoxNTU0NzcyNjAzLCJpZGVudGl0eSI6ImFkbWluIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.gswW8M3w7WxebyPzHAdiJgqxJ_2TuF4u_K6-HECNFD0"
    }
    
    > set ACCESS=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NTQ3NzE3MDMsIm5iZiI6MTU1NDc3MTcwMywianRpIjoiZmYyZmQ4OTAtZDlmYy00NDIxLWEyMWQtNTUxMTczMGNhOGY0IiwiZXhwIjoxNTU0NzcyNjAzLCJpZGVudGl0eSI6ImFkbWluIiwiZnJlc2giOmZhbHNlLCJ0eXBlIjoiYWNjZXNzIn0.gswW8M3w7WxebyPzHAdiJgqxJ_2TuF4u_K6-HECNFD0
    > curl -H "Authorization: Bearer %ACCESS%" "http://localhost:5000/api/repositories/import?drop=true"
    {
        "message": "API rate limit exceeded for ***.***.***.***. (But here's the good news: Authenticated requests get a higher rate limit. Check out the documentation for more details.)",
        "documentation_url": "https://developer.github.com/v3/#rate-limiting",
        "fetched_repos": 300
    }
    
    > curl "http://localhost:5000/api/repositories/100"
    {
        "id": 100,
        "full_name": "matplotlib/matplotlib",
        "html_url": "https://github.com/matplotlib/matplotlib",
        "description": "matplotlib: plotting with Python",
        "stargazers_count": 9011,
        "language": "Python"
    }
