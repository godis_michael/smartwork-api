@echo off

set FLASK_APP=flaskr
set FLASK_ENV=development
set FLASK_SKIP_DOTENV=1

flask run