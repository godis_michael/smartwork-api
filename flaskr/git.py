from decouple import config
from github import Github


def find_repos(stars:str, language:str, *credentials:list):
    """
    Find github repositories by given language name and with rating more than 500 stars.

    :param stars: number of github stars (rating)
    :param language: programming language
    :param credentials: github access token or username with password (not required)
    :return: found repositories
    """
    if len(credentials) > 2:  # possible options: no credentials / access token / username and password
        raise ConnectionError('Wrong credentials number. '
                              'Use access token, username and password  or don`t use them at all.')

    gh = Github(*credentials)
    return gh.search_repositories(query=f'stars:>{stars} language:{language}')


def get_credentials():
    """
    Look for github access token or username and password in .env file.

    :return: credentials or empty list if nothing found
    """
    GITHUB_ACCESS_TOKEN = config('GITHUB_ACCESS_TOKEN', default=None)
    if GITHUB_ACCESS_TOKEN:
        return GITHUB_ACCESS_TOKEN,

    GITHUB_USERNAME = config('GITHUB_USERNAME', default=None)
    GITHUB_PASSWORD = config('GITHUB_PASSWORD', default=None)
    if GITHUB_USERNAME and GITHUB_PASSWORD:
        return GITHUB_USERNAME, GITHUB_PASSWORD

    return []
