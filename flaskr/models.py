from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import check_password_hash

# instance for db interaction
db = SQLAlchemy()


class User(db.Model):
    """
    Model that represents superuser.
    This user may then fulfill database by calling /api/repositories/import endpoint.
    """
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True, nullable=False)
    password = db.Column(db.String(128), nullable=False)

    @classmethod
    def find_user(cls, username:str):
        """
        Find user by username.

        :param username: user login
        :return: User or None
        """
        return cls.query.filter_by(username=username).first()

    def verify_hash(self, password:str):
        """
        Check if password matches.

        :param password: user password
        :return: True or False
        """
        return check_password_hash(self.password, password)

    def save(self):
        """
        Add instance to a database and save it.
        """
        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '<User: %r>' % self.username


class Repository(db.Model):
    """
    Model that represents github repository.
    """
    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(80), nullable=False)
    html_url = db.Column(db.String(2083), unique=True, nullable=False)
    description = db.Column(db.Text)
    stargazers_count = db.Column(db.Integer, nullable=False)
    language = db.Column(db.String(80), nullable=False, default='python')

    def as_dict(self):
        """
        Convert Repository instance to the dict structure.

        :return: instance data in dictionary
        """
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    # save method is not implemented here
    # usually we need to commit a lot of repositories at once, therefore this has no sense

    def __repr__(self):
        return '<Repository: %r>' % self.full_name
