import flask_sqlalchemy as sa
from flask import Flask, Blueprint, redirect
from flask_jwt_extended import create_access_token, jwt_required
from flask_restful import Api, Resource, reqparse
from github import RateLimitExceededException

from .models import Repository, User, db
from .git import find_repos, get_credentials

__all__ = ['init_app']


class UserLogin(Resource):
    """
    POST /api/login

    :param username: superuser name
    :param password: superuser password
    """
    def post(self):
        data = self.parse()
        current_user = User.find_user(data['username'])

        if not current_user:
            return {'message': 'User {} doesn\'t exist'.format(data['username'])}

        if current_user.verify_hash(data['password']):
            access_token = create_access_token(identity=data['username'])
            return {'message': f'Logged in as {current_user.username}',
                    'access_token': access_token}
        else:
            return {'message': 'Wrong credentials'}

    @staticmethod
    def parse():
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('username', location='form', required=True, help='This field cannot be blank')
        parser.add_argument('password', location='form', required=True, help='This field cannot be blank')
        return parser.parse_args()


class RepositoriesList(Resource):
    """
    GET /api/repositories

    :param sort: sort data by some field (id by default)
    :param order: make ascending or descending order (asc by default)
    :param page: paginate results, 10 entries are shown per page (1st page by default)
    """
    def get(self):
        data = self.parse()
        obj_start, obj_end = data['page'] * 10 - 10, data['page'] * 10  # objects indices
        total_num = Repository.query.count()  # total count of objects in db
        items = []

        # check if indices are in allowed bounds
        if  obj_start < 0 or obj_end > total_num:
            return {'message': '"page" value is out of possible bounds',
                    'min_page': 0, 'max_page': total_num // 10}

        params = eval(f'Repository.{data["sort"]}.{data["order"]}()')  # get sorting parameters
        repositories = Repository.query.order_by(params).limit(obj_end).all()

        # should never happen
        if not repositories:
            return {'message': 'Could not find objects with such parameters'}

        # convert instances to serializable format
        for r in repositories[obj_start: ]:
            items.append(r.as_dict())

        return items

    @staticmethod
    def parse():
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('sort', location='args', type=sort, default='id', help='"sort" value is not valid')
        parser.add_argument('order', location='args', type=order, default='asc', help='"order" value is not valid')
        parser.add_argument('page', location='args', type=int, default=1, help='"page" value is not valid')
        return parser.parse_args()


class RepositoryEntry(Resource):
    """
    GET /api/repositories/<id>
    """
    def get(self, id):
        try:
            id = int(id)
        except:
            return {'message': 'Invalid "id" value'}

        return Repository.query.get_or_404(id).as_dict()


class RepositoriesImport(Resource):
    """
    GET /api/repositories/import
    This endpoint requires access token generated from /api/login to be set in Authorization header, like:
        Authorization: Bearer <access_token>

    :param drop: if True, db will be recreated if not empty
    :param stars: repositories rating for a search
    :param language: repositories language for a search
    """
    @jwt_required
    def get(self):
        data = self.parse()
        table_names = sa.inspect(db.engine).get_table_names()
        credentials = get_credentials()
        fetched_repos = 0

        if table_names:  # if there are already some tables
            if data['drop']:  # clear all data from db
                db.drop_all()
                db.create_all()
            else:
                return {'message': 'Database is not empty, could not import. '
                                   'Send this request with "drop" parameter to recreate db.'}

        try:
            for r in find_repos(data['stars'], data['language'], *credentials):
                repository = Repository(full_name = r.full_name,
                                        html_url = r.html_url,
                                        description = r.description,
                                        stargazers_count = r.stargazers_count,
                                        language = r.language)
                db.session.add(repository)
                fetched_repos += 1
        except RateLimitExceededException as e:  # stop if connection limit received
            db.session.commit()
            err = e.data
            err['fetched_repos'] = fetched_repos
            return err

        db.session.commit()
        return redirect('/api/repositories')

    @staticmethod
    def parse():
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('drop', location='args', type=bool, default=False, help='Drop tables if exist')
        parser.add_argument('stars', location='args', type=int, default=500, help='Repository stars amount')
        parser.add_argument('language', location='args', default='python', help='Searched programming language')
        return parser.parse_args()


def sort(value:str):
    """
    sort parameter validator for RepositoriesImport resource.

    :param value: user input
    :return: value itself if no exception raised
    """
    if value in ('id', 'full_name', 'html_url', 'description', 'language'):
        return value
    raise ValueError(f'Wrong sorting option: {value}')


def order(value:str):
    """
        order parameter validator for RepositoriesImport resource.

        :param value: user input
        :return: value itself if no exception raised
        """
    if value in ('asc', 'desc'):
        return value
    raise ValueError(f'Wrong order option: {value}')


def init_app(app:Flask):
    """
    Register api endpoints.

    :param app: flask application instance
    """
    bp = Blueprint('api', __name__, url_prefix='/api')
    api = Api(bp)

    api.add_resource(UserLogin, '/login')
    api.add_resource(RepositoriesList, '/repositories')
    api.add_resource(RepositoryEntry, '/repositories/<id>')
    api.add_resource(RepositoriesImport, '/repositories/import')

    app.register_blueprint(bp)
