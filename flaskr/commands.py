import getpass
import re

import click
import sqlalchemy as sa
from flask import Flask
from flask.cli import with_appcontext
from werkzeug.security import generate_password_hash

from github import RateLimitExceededException

from .git import find_repos, get_credentials
from .models import Repository, User, db

__all__ = ['init_app']


@click.command('init-db')
@with_appcontext
def init_db_command():
    """
    Create tables in a database.
    If tables already exist (db not empty), a prompt is shown to confirm recreation.
    """
    table_names = sa.inspect(db.engine).get_table_names()

    # if there are already some tables, ask for recreation
    if table_names:
        if click.confirm(f'{table_names} tables already exist, drop them?', default=True):
            db.drop_all()
        else:
            click.echo('Cancelled', err=True)
            return

    # else simply create tables
    db.create_all()
    click.echo('Initialized the database')


@click.command('create-superuser')
@with_appcontext
@click.option('--username', prompt=True, default=lambda: getpass.getuser())
@click.password_option()
def create_superuser_command(username:str, password:str):
    """
    Create superuser, which then may be used from api endpoint to import repositories from github.

    :param username: superuser login
    :param password: superuser password
    """
    error = None

    # check username and password for validness
    if not username or username.find(' ') != -1:
        error = f'{username} username is not valid'
    elif not (re.search(r'\d', password) and re.search(r'[a-z]', password)):
        error = 'Password should contain at least one digit and one letter'
    elif len(password) < 8:
        error = 'Password length should be more than 8 characters'
    elif User.find_user(username) is not None:
        error = 'User {} is already registered'.format(username)

    # if no errors, create new user
    if error is None:
        user = User(username=username,
                    password=generate_password_hash(password))
        user.save()
        click.echo('Successfully created')
        return

    # else echo error
    click.echo(error, err=True)


@click.command('import')
@with_appcontext
@click.option('--stars', default='500')
@click.option('--language', default='python')
def import_command(stars:str, language:str):
    """
    Load repositories data into db.

    :param stars: rating of repositories that should be found
    :param language: programming language of repositories
    """
    credentials = get_credentials()
    fetched_repos = 0

    try:
        with click.progressbar(find_repos(stars, language, *credentials)) as bar:
            for r in bar:
                repository = Repository(full_name = r.full_name,
                                        html_url = r.html_url,
                                        description = r.description,
                                        stargazers_count = r.stargazers_count,
                                        language = r.language)
                db.session.add(repository)
                fetched_repos += 1
    except RateLimitExceededException as e:  # stop if connection limit received
        click.echo(f'Warning! {e.data["message"]} ({e.data["documentation_url"]})', err=True)

    # save data in db
    if fetched_repos:
        db.session.commit()
    click.echo(f'{fetched_repos} entries successfully added')


def init_app(app: Flask):
    """
    Add cli commands to the application.

    :param app: flask application instance
    """
    app.cli.add_command(init_db_command)
    app.cli.add_command(create_superuser_command)
    app.cli.add_command(import_command)
