import os

from flask import Flask
from flask_jwt_extended import JWTManager

from decouple import config


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)  # create application
    # set initial configuration
    app.config.from_mapping(
        SECRET_KEY=config('SECRET_KEY'),
        JWT_SECRET_KEY=config('JWT_SECRET_KEY'),
        SQLALCHEMY_DATABASE_URI='sqlite:///' + os.path.join(app.instance_path, f'{config("DATABASE")}.sqlite'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
    )

    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # set manager instance for JWT
    jwt = JWTManager(app)

    # configure database
    from .models import db
    db.init_app(app)

    # configure flask cli command
    from . import commands
    commands.init_app(app)

    # configure api endpoints
    from . import resources
    resources.init_app(app)

    return app
